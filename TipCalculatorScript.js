$( document ).ready(function() {
$('.addPercent').click(function () {
	var tipPercent = $(this).prev().val();
	if(tipPercent.substr(tipPercent.length-1,1)=="%")
	{
		tipPercent = tipPercent.substr(0, tipPercent.length-1);
	}
	tipPercent=(parseFloat(tipPercent) + 1+"%");
	$(this).prev().val(tipPercent);
	total();
});
$('.subPercent').click(function () {
    var tipPercent = $(this).next().val();
	if(tipPercent.substr(tipPercent.length-1,1)=="%")
	{
		tipPercent = tipPercent.substr(0, tipPercent.length-1);
	}
	tipPercent=(parseFloat(tipPercent) -1+"%");
	$(this).next().val(tipPercent);
	total();
});
$('.add').click(function () {
    $(this).prev().val(+$(this).prev().val() + 1);
	total();
});
$('.sub').click(function () {
    if ($(this).next().val() > 0) $(this).next().val(+$(this).next().val() - 1);
	total();
});


$('#calculate').click(function (){	
	total();
});

function total(){
	var bill = $("#bill").val();
	if(bill.substring(0,1)=="$")
	{
		var bill = bill.substring(1);
	}
	var tipPercent = $("#tip").val()
	if(tipPercent.substr(tipPercent.length-1,1)=="%")
	{
		tipPercent = tipPercent.substr(0, tipPercent.length-1);
	}
	var tip = bill*tipPercent/100/$("#people").val();
	var total = bill/$("#people").val()+tip;
	var fullAmount = tip + total;
	var tip = "$" + tip.toFixed(2);
	var total = "$" + total.toFixed(2);
	var fullAmount = "$" + fullAmount.toFixed(2);
	$("#tipPerPerson").html(tip);	
	$("#totalPerPerson").html(total);
	$("#fullAmount").text(fullAmount);
	
};




});


